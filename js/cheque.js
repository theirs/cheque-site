// prevents flickering of css transitions on page load
$(window).load(function() {
	$('body').removeClass('preload');
});

if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {	

}
else {
	var element = document.getElementById('videoSection');
	element.parentNode.removeChild(element);		
}

var currentSection = 0;

$('.scrollIndicator').click(function(){
	currentSection = $(this).parent().index() + 1;
	changeSection();
});

$('.sectionIndicator').click(function() {
	currentSection = $(this).index();
	changeSection();
});

$(document).keydown(function(e) {
    
    if (e.which == 37 || e.which == 38) {
    	move('down');
    }
    else if (e.which == 39 || e.which == 40) {
    	move('up');
    }

    changeSection();
});


var justScrolled = false;

window.addEventListener('mousewheel', function(e){

	if (!justScrolled) {

		justScrolled = true;

		setTimeout(function() {
			justScrolled = false;
		}, 1500);

		wDelta = e.wheelDelta < 0 ? 'down' : 'up';
		
		if (wDelta == 'down') {
			move('up');
		}
		
		else if (wDelta == 'up') {		
			move('down');
		}		

	}

});

$('body').on('swipedown',function(){
	move('down');
});

$('body').on('swipeup', function() {
	move('up');
});

var numberOfSections = $('.section').length;

function move(direction) {

	if (direction == 'up' && currentSection < numberOfSections-1) {
		currentSection++;
		changeSection();
	}
	else if (direction == 'down' && currentSection > 0) {
		currentSection--;
		changeSection();
	}	
};

function changeSection() {

	$('.section').each(function() {
		$(this).addClass('down');
		if ($(this).index() < currentSection) {
			$(this).addClass('up');
			$(this).removeClass('down');
		}
		else if ($(this).index() == currentSection) {
			$(this).removeClass('up');
			$(this).removeClass('down');
			$('body')[0].className = 'active-' + $(this)[0].className.split('section-')[1];
		}
	});
	
	$('.sectionIndicator').removeClass('active');
	$($('.sectionIndicator').get(currentSection)).addClass('active');

};

function goToBeginning() {
	currentSection = 0;
	changeSection();
};
